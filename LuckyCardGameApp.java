import java.util.Scanner;
public class LuckyCardGameApp{
	public static void main(String[] args){
		Scanner reader = new Scanner(System.in);
		Deck deck = new Deck();
		deck.shuffle();

		int amountToRemove = 0;
		System.out.println("hello how many cards to remove? ");
		amountToRemove = Integer.parseInt(reader.nextLine());

		while(amountToRemove < 0 || amountToRemove > deck.length()){
			System.out.println("hello how many cards to remove? ");
			amountToRemove = Integer.parseInt(reader.nextLine());
		}

		System.out.println("Before: " + deck.length() + " cards.");
		int i = 0;
		while(i < amountToRemove){
			deck.drawTopCard();
			i++;
		}
		System.out.println("After: " + deck.length() + " cards.");

		deck.shuffle();
		System.out.println(deck);
	}
}
