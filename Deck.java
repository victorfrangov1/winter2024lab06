import java.util.Random;
public class Deck {
  private Card[] cards;
  private int numberOfCards;
  private Random rng;

  public Deck() {
    String[] suits = new String[]{ "hearts", "spades", "clubs", "diamonds" };
    String[] value = new String[]{ "A", "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K" };
    this.numberOfCards = 52;
    this.rng = new Random();
    this.cards = new Card[52];

		int i = 0;
    for (int j = 0; j < suits.length; j++) {
      for (int k = 0; k < value.length; k++) {
        this.cards[i] = new Card(suits[j], value[k]);
        i++;
      }
    }
  }

  public int length() {
    return this.numberOfCards;
  }

  public String drawTopCard() {
    this.numberOfCards--;
    return this.cards[this.numberOfCards].getSuit() + this.cards[this.numberOfCards].getValue();
  }

  public String toString() {
			String result = "";
    for (int i = 0; i < numberOfCards; i++) {
      result += this.cards[i] + "\n";
    }
    return result;
  }

  public void shuffle() {
    for (int i = 0; i < this.numberOfCards - 1; i++) {
				int pos = this.rng.nextInt(this.numberOfCards - 1);

				Card temp = this.cards[i];
      this.cards[i] = this.cards[pos];
      this.cards[pos] = temp;
    }
  }
}
